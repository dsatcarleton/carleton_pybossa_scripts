"""
Parse alum names into a JSON formatted list for use in a PyBossa project

Usage: python3 filename

where filename is a tab-separated text file representing a table with the headings 
FIRST_NAME LAST_NAME PREF_MAIL_NAME BIRTH_LAST_NAME PREF_CLASS_YEAR
as provided by the archives

Output: filename_list.json
"""

import sys
import json

fname = sys.argv[1]

lines = []
with open(fname) as f:
	file = f.read()
	file.replace('\n', '\r')
	lines = file.split("\r")

lines.pop(0) # this removes the heading line in the tab-separated file
names = []
for line in lines:
	name = ""
	name_data = line.split("\t")
	name += name_data[0] + " " + name_data[1]	# Add first name and last name
	if name_data[3]:
		name += " (" + name_data[3] + ")" 		# Add birth last name in parentheses if listed
	names.append(name)

filename = fname[:-4]
with open(filename+"_list.json", "w") as f:
	f.write(json.dumps(names, indent=4))
