import face_recognition
import sys
import subprocess
import os
from PIL import Image
import requests
from io import BytesIO
import csv
import numpy


#Functions return True on success, and False if there's an error.
def draw_faces(path,faces,reunion_year,url=None):
    #Get height and width of orginal image
    if(url!=None):
        try:
            dur=subprocess.Popen("curl -s '"+url+"' | identify -quiet -format '%w,%h' fd:0",shell=True,stdout=subprocess.PIPE).stdout
            width,height=dur.read().split(b',')
            height=int(height)
            width=int(width)
        except:
            print("Error: Can't find image "+url)
            return False
    else:
        try:
            dur=subprocess.Popen("identify -quiet -format '%w,%h' '"+path+"'",shell=True,stdout=subprocess.PIPE).stdout
            width,height=dur.read().split(b',')
            height=int(height)
            width=int(width)
        except:
            print("Error: Can't find image "+path)
            return False
    extension=path.split('.')[-1]
    if(path==extension):
        extension=''
    else:
        extension='.'+extension

    #Prevent duplicates, and append the _num before the extension
    num=1
    outpath=''.join(path.split('.')[:-1])
    for face in faces:
        while(os.path.isfile(outpath+"_"+str(num)+extension)):
            num+=1
        draw_corners(face,height,width,path,reunion_year+"/"+outpath+"_"+str(num)+extension,url) #Might want to check return of this.
        num+=1
    return True

#Helper function, draws given rectangle on given image. locations is tuple (top,left,bottom,right). Origin (0,0) is top left of image.
def draw_corners(locations,maxheight,maxwidth,inpath,outpath,url=None):
    #These make more sense as hardcoded constants, I think. Adjust as needed.
    margin = 10
    strokewidth = 3
    color = "'#0000ff'"

    #Ensures that top left is always smallest values minus (10,10), and bottom right is largest plus (10,10)
    top=max(0,min(locations[0],locations[2])-margin)
    bottom=min(maxheight,max(locations[0],locations[2])+margin)
    left=max(0,min(locations[1],locations[3])-margin)
    right=min(maxwidth,max(locations[1],locations[3])+margin)
    if(url!=None):
        base="curl -s '"+url+"' |"
        inpath="fd:0"
    else:
        base=""
    ret = os.system(base+"convert -quiet -strokewidth "+str(strokewidth)+" -stroke "+color+" -fill none -draw 'rectangle "+"{1},{0} {3},{2}".format(top,left,bottom,right)+"' '"+inpath+"' '"+outpath+"'")
    if(ret):
        print("Error: Can't draw face in image "+outpath)
        return False
    return True

def find_and_draw_url(url, filename, reunion_year, upsample):
    response = requests.get(url)
    image = Image.open(BytesIO(response.content))
    image_matrix = numpy.array(image)
    faces = face_recognition.face_locations(image_matrix, number_of_times_to_upsample=upsample, model="cnn")
    draw_faces(filename, faces, reunion_year, url)

def find_and_draw_csv(csv_filename, upsample):
    file = open(csv_filename, 'r')
    csv_file = csv.reader(file)
    header = True
    for line in csv_file:
        if header:
            header = False
            continue
        image_filename = line[4]
        image_url = line[6]
        reunion_folder = image_url.split("/")[4]
        reunion_year = reunion_folder.split("_")[1]
        find_and_draw_url(image_url, image_filename, reunion_year, upsample)

if __name__ == "__main__":
    find_and_draw_csv("FacialRecognitionImagesList_TestSet.csv",2)
    #test_csv("FacialRecognitionImagesList_TestSet.csv")
