#!/bin/sh

docker cp $1 pybossa:/opt/pybossa/$2
docker exec -u root pybossa sh -c "cd $2 &&
pbs --server http://137.22.30.2:8080 --api-key $3 add_tasks --tasks-file $1
pbs --server http://137.22.30.2:8080 --api-key $3 update_project"
