#!/bin/sh

docker exec -u root pybossa pip install pybossa-pbs
echo "finished installing pbs"
docker exec -u root pybossa git clone $1
echo "cloned git repo"
docker exec -u root pybossa sh -c "cd $2 &&
touch results.html &&
pbs --server http://137.22.30.2:8080 --api-key $3 create_project &&
pbs --server http://137.22.30.2:8080 --api-key $3 update_project"
