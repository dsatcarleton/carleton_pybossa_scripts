#!/bin/sh

docker exec -u root pybossa pip install pybossa-pbs
echo "finished installing pbs"
docker cp $1 pybossa:/opt/pybossa/
echo "copied project folder"
docker exec -u root pybossa sh -c "cd $1 &&
touch results.html &&
pbs --server http://137.22.30.2:8080 --api-key $2 create_project &&
pbs --server http://137.22.30.2:8080 --api-key $2 update_project"
